#include "init.h"
#include <iostream>

void initVertexData(VertexEntry* init_data, vertexid_t vertex, Graph* graph) {
    init_data->data.parent = 0;
    init_data->data.visited = false;
    init_data->data.active = false;
    if (vertex == 0) {
        init_data->data.visited = true;
        init_data->data.active = true;
    }
}

void printFinalResult(VertexEntry* init_data, int num_vertices) {
    bool first = true;
    int i;
    for (i = 0; i < num_vertices; i++) {
        if (!init_data[i].data.visited) {
            if (first) {
                std::cout << "Unvisited vertices:";
                first = false;
            }
            std::cout << " " << init_data[i].id;
        }
    }
    if (!first) {
        std::cout << std::endl;
    }
}
