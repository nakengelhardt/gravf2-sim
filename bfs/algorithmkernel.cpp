#include "algorithmkernel.h"
#include <iostream>

void receive(VertexEntry* vertex, Message* message) {
    if (!vertex->data.visited) {
        std::cout << "Visiting vertex " << vertex->id << " and setting parent to " << message->sender << '\n';
        vertex->data.parent = message->sender;
        vertex->data.visited = true;
        vertex->data.active = true;
    }
}

bool send(Update* update, VertexEntry* vertex) {
    if (vertex->data.active) {
        std::cout << "Send from vertex " << vertex->id << '\n';
        update->sender = vertex->id;
        vertex->data.active = false;
        return true;
    }
    return false;
}

bool distribute(Message* message, Update* update, edge_t edge) {
    std::cout << "Message from vertex " << update->sender << " to vertex " << edge.dest_id << '\n';
    message->sender = update->sender;
    message->dest_id = edge.dest_id;
    return true;
}
