#include "init.h"
#include <iostream>

float const_base;

void initVertexData(VertexEntry* init_data, vertexid_t vertex, Graph* graph) {
    const_base = 0.15/graph->nv;
    init_data->data.weight = 0.15/graph->nv;
    init_data->data.sum = 0.15/graph->nv;
    init_data->data.nneighbors = graph->num_neighbors(vertex);
}

void printFinalState(VertexEntry* init_data) {
    std::cout << init_data->id << ": " << init_data->data.weight << std::endl;
}
