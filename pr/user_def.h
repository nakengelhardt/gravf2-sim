#pragma once

#include <inttypes.h>

typedef int64_t vertexid_t;

struct edge_t {
    vertexid_t dest_id;
};

struct VertexData {
    float sum;
    float weight;
    int nneighbors;
};

struct MessagePayload {
    float weight;
};

struct UpdatePayload {
    float weight;
};

const bool has_edgedata = false;

extern float const_base;
