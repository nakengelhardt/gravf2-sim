#include "algorithmkernel.h"
#include <iostream>

void receive(VertexEntry* vertex, Message* message) {
    vertex->data.sum += message->payload.weight;
}

bool send(Update* update, VertexEntry* vertex) {
    if (superstep < 30) {
        update->sender = vertex->id;
        vertex->data.weight = vertex->data.sum;
        update->payload.weight = vertex->data.weight/vertex->data.nneighbors;
        vertex->data.sum = const_base;
        return true;
    }
    return false;
}

bool distribute(Message* message, Update* update, edge_t edge) {
    message->sender = update->sender;
    message->dest_id = edge.dest_id;
    message->payload.weight = update->payload.weight;
    return true;
}
