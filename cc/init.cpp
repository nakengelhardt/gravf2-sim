#include "init.h"
#include <iostream>

void initVertexData(VertexEntry* init_data, vertexid_t vertex, Graph* graph) {
    init_data->data.color = vertex;
    init_data->data.active = true;
}

void printFinalState(VertexEntry* init_data) {
    std::cout << init_data->id << ": " << init_data->data.color << std::endl;
}
