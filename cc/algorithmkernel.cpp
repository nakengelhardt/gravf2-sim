#include "algorithmkernel.h"
#include <iostream>

void receive(VertexEntry* vertex, Message* message) {
    if(vertex->data.color > message->payload.color) {
        // std::cout << "Visiting vertex " << vertex->id << " and setting color to " << message->payload.color << '\n';
        vertex->data.color = message->payload.color;
        vertex->data.active = true;
    }
}

bool send(Update* update, VertexEntry* vertex) {
    if (vertex->data.active) {
        update->sender = vertex->id;
        update->payload.color = vertex->data.color;
        vertex->data.active = false;
        return true;
    }
    return false;
}

bool distribute(Message* message, Update* update, edge_t edge) {
    message->sender = update->sender;
    message->dest_id = edge.dest_id;
    message->payload.color = update->payload.color;
    return true;
}
