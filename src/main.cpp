#include "graph.h"
#include "init.h"
#include "algorithmkernel.h"


#include <iostream>
#include <stdexcept>
#include <string>
#include <chrono>

int superstep;

int main(int argc, char **argv, char **env) {
    char const * gname = "../fpgagraphlib/data/s4e4";
    int sz = 64;

    if (argc > 2) {
        gname = argv[1];
        sz = std::stoi(argv[2]);
    }
    std::cout << "Loading graph from "<< gname << '\n';

    // Graph* graph = new Graph("../../data/s11e16", 32768);
    // Graph* graph = new Graph("../../data/s13e16", 131072);
    // Graph* graph = new Graph("../../data/s14e16", 262144);
    Graph* graph = new Graph(gname, sz, false);

    std::cout << "Graph has " << graph->nv << " vertices and " << graph->ne << " edges." << std::endl;

    if(graph->nv < 30){
        for (int in_slice = 0; in_slice < num_slices; in_slice++) {
            std::cout << "Slice " << in_slice << "\n";
            for(int i = 0; i < graph->partition->vertices_in_slice[in_slice]; i++){
                vertexid_t v = (in_slice << PEID_SHIFT) + i;
                std::cout << "Vertex " << v << ": ";
                for (int out_slice = 0; out_slice < num_slices; out_slice++) {
                    if (out_slice != 0) { std::cout << "| "; }
                    int num_neighbors = graph->num_neighbors(v, out_slice);
                    // std::cout << "(" << num_neighbors << ") " << std::flush;
                    for(int j = 0; j < num_neighbors; j++){
                        std::cout << graph->get_neighbor(v, j, out_slice).dest_id << " ";
                    }
                }
                std::cout << "\n";
            }
        }
        graph->print_dot("last_graph.dot");
    }

    auto t_start = std::chrono::high_resolution_clock::now();

    int max_vertices_in_slice = graph->partition->max_vertices_in_slice;

    VertexEntry* vertices[num_slices];
    Update* updates[num_slices];

    // initialize vertex data
    for (int slice = 0; slice < num_slices; slice++) {
        vertices[slice] = new VertexEntry[max_vertices_in_slice];
        updates[slice] = new Update[max_vertices_in_slice];
        for(int i = 0; i < max_vertices_in_slice; i++){
            vertices[slice][i].id = -42;
            vertexid_t vertex = graph->partition->origin(slice, i);
            if ( vertex < graph->nv && vertex >= 0 ) {
                vertices[slice][i].id = vertex;
                initVertexData(&vertices[slice][i], vertex, graph);
            }
        }
    }

    // iterate until inactive
    bool activity;
    superstep = 0;
    int total_messages = 0;
    int total_updates = 0;
    do {
        activity = false;
        int num_messages = 0;
        int num_updates[num_slices] = {0};
        #pragma omp parallel for reduction(+:total_updates)
        for (int in_slice = 0; in_slice < num_slices; in_slice++) {
            // generate updates
            for (int i = 0; i < graph->partition->vertices_in_slice[in_slice]; i++) {
                if (send(&updates[in_slice][num_updates[in_slice]], &vertices[in_slice][i])) {
                    // std::cout << "Send from vertex " << graph->partition->placement(vertices[in_slice][i].id) << '\n';
                    num_updates[in_slice]++;
                    total_updates++;
                }
            }
        }
        #pragma omp parallel for reduction(+:num_messages, total_messages) reduction(|:activity)
        for (int out_slice = 0; out_slice < num_slices; out_slice++){
            for (int in_slice = 0; in_slice < num_slices; in_slice++) {
            // generate and deliver messages
                for (int i = 0; i < num_updates[in_slice]; i++) {
                    Message message;
                    vertexid_t sender = updates[in_slice][i].sender;
                    int num_neighbors = graph->num_neighbors(sender, out_slice);
                    for (int j = 0; j < num_neighbors; j++){
                        edge_t neighbor = graph->get_neighbor(sender, j, out_slice);
                        if (distribute(&message, &updates[in_slice][i], neighbor)){
                            // std::cout << "Message from vertex " << updates[in_slice][i].sender << " to vertex " << neighbor.dest_id << '\n';
                            activity |= true;
                            num_messages++;
                            total_messages++;
                            int dest_id = message.dest_id;
                            int dest_slice = graph->partition->slice_id(dest_id);
                            int dest_local = graph->partition->local_id(dest_id);
                            if (dest_slice != out_slice) {
                                std::cerr << "in_slice = " << in_slice << ", out_slice = " << out_slice << ", num_neighbors = " << num_neighbors << ", j = " << j << "\n";
                                std::cerr << sender << " -> " << dest_id << "[" << dest_slice << ":" << dest_local << "]" << std::endl;
                                throw std::runtime_error("wrong slice");
                            }
                            receive(&vertices[dest_slice][dest_local], &message);
                        }
                    }
                }
            }
        }

        superstep++;
        // std::cout << "Superstep " << superstep << ": " << num_messages << " messages" << '\n';
        // std::cin.ignore();
    } while (activity);

    auto t_end = std::chrono::high_resolution_clock::now();
    double elaspedTimeMs = std::chrono::duration<double, std::milli>(t_end-t_start).count();

    std::cout << total_updates << " updates and " << total_messages << " messages issued in " << superstep << " supersteps." << '\n';
    std::cout << "Elapsed time: " << elaspedTimeMs << " ms.\n";

    if(graph->nv < 30){
        std::cout << "Final state:\n";
        for (int slice = 0; slice < num_slices; slice++) {
            for(int i = 0; i < max_vertices_in_slice; i++){
                vertexid_t vertex = graph->partition->origin(slice, i);
                if ( vertex < graph->nv && vertex >= 0 ) {
                    printFinalState(&vertices[slice][i]);
                }
            }
        }
    }

    for (int slice = 0; slice < num_slices; slice++) {
        delete[] vertices[slice];
        delete[] updates[slice];
    }
    delete graph;

    return 0;
}
