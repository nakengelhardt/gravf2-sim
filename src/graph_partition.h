#pragma once

#include "format_def.h"

class GraphPartition {
public:
    vertexid_t vertices_in_slice[num_slices] = {};
    vertexid_t max_vertices_in_slice = 0;
    GraphPartition(int num_vertices);
    vertexid_t placement(vertexid_t vertex);
    vertexid_t origin(vertexid_t vertex);
    vertexid_t origin(int slice_id, vertexid_t local_id);
    int slice_id(vertexid_t vertex);
    int local_id(vertexid_t vertex);
};
