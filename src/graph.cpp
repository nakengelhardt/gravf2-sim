/* CSR graph storage adapted from graph500 benchmark; portions of this code are Copyright 2010-2011,  Georgia Institute of Technology, USA. See https://github.com/graph500/graph500 */
#include "graph.h"

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <fstream>
#include <fcntl.h>
#include <unistd.h>

Graph::Graph(const char* dumpname, int64_t nedge, bool directed) : directed(directed) {
    packed_edge* IJ = new packed_edge[nedge];

    int fd;
    ssize_t sz;
    if ((fd = open (dumpname, O_RDONLY)) < 0) {
        throw std::runtime_error("Cannot open input graph file");
    }
    sz = nedge * sizeof (*IJ);
    if (sz != read (fd, IJ, sz)) {
        throw std::runtime_error("Error reading input graph file");
    }
    close (fd);

    find_nv(IJ, nedge);
    partition = new GraphPartition(nv);

    int i, j;
    for (i = 0; i < num_slices; i++) {
        for (j = 0; j < num_slices; j++) {
            shard[i][j] = new Shard(i, j, partition->vertices_in_slice[i]);
        }
    }

    setup_deg_off (IJ, nedge);
    gather_edges (IJ, nedge);
    if (has_edgedata){
        populate_edgedata();
    }

    delete[] IJ;
}

Graph::~Graph(){
    delete partition;
    int i, j;
    for (i = 0; i < num_slices; i++) {
        for (j = 0; j < num_slices; j++) {
            delete shard[i][j];
        }
    }
}

void Graph::find_nv (const packed_edge* IJ, const int64_t nedge) {
    int64_t k;

    maxvtx = -1;
    for (k = 0; k < nedge; ++k) {
        if (IJ[k].v0 > maxvtx) {
            maxvtx = IJ[k].v0;
        }
        if (IJ[k].v1 > maxvtx) {
            maxvtx = IJ[k].v1;
        }
    }
    nv = 1+maxvtx;
}



void Graph::setup_deg_off (const packed_edge* IJ, int64_t nedge) {
    max_edges_in_shard = 0;
    int64_t k, accum;
    int in_slice, out_slice;

    // initialize counts
    for (in_slice = 0; in_slice < num_slices; in_slice++) {
        for (out_slice = 0; out_slice < num_slices; out_slice++) {
            for (k = 0; k < partition->vertices_in_slice[in_slice]; ++k){
                shard[in_slice][out_slice]->xoff[k] = 0;
                shard[in_slice][out_slice]->xendoff[k] = 0;
            }
        }
    }

    // count degree for all vertices
    for (k = 0; k < nedge; ++k) {
        vertexid_t i = partition->placement(IJ[k].v0);
        vertexid_t j = partition->placement(IJ[k].v1);
        if (i >= 0 && j >= 0 && i != j) { /* Skip self-edges. */
            shard[partition->slice_id(i)][partition->slice_id(j)]->xoff[partition->local_id(i)]++;
            if (!directed) shard[partition->slice_id(j)][partition->slice_id(i)]->xoff[partition->local_id(j)]++;
        }
    }

    // shift indexes
    for (in_slice = 0; in_slice < num_slices; in_slice++) {
        for (out_slice = 0; out_slice < num_slices; out_slice++) {
            accum = 0;
            for (k = 0; k < partition->vertices_in_slice[in_slice]; ++k){
                int tmp = shard[in_slice][out_slice]->xoff[k];
                shard[in_slice][out_slice]->xoff[k] = accum;
                shard[in_slice][out_slice]->xendoff[k] = accum; // for now all edgelists are empty
                accum += tmp;
            }

            if (max_edges_in_shard < accum) max_edges_in_shard = accum;

            // set up edge list storage
            shard[in_slice][out_slice]->xadj = new edge_t[accum];
            for (k = 0; k < accum; ++k) {
                shard[in_slice][out_slice]->xadj[k].dest_id = -42;
            }
        }
    }

}

void Graph::scatter_edge (const vertexid_t i, const vertexid_t j) {
    int in_slice, out_slice;
    in_slice = partition->slice_id(i);
    out_slice = partition->slice_id(j);
    int local_id = partition->local_id(i);
    shard[in_slice][out_slice]->xadj[shard[in_slice][out_slice]->xendoff[local_id]].dest_id = j;
    shard[in_slice][out_slice]->xendoff[local_id]++;
}

static int edge_cmp (const void *a, const void *b)
{
    const vertexid_t ia = ((const edge_t*)a)->dest_id;
    const vertexid_t ib = ((const edge_t*)b)->dest_id;
    if (ia < ib) return -1;
    if (ia > ib) return 1;
    return 0;
}

vertexid_t Graph::pack_vtx_edges (const vertexid_t i)
{
    vertexid_t num_neighbors = 0;
    int in_slice, out_slice;
    in_slice = partition->slice_id(i);
    int local_id = partition->local_id(i);
    for (out_slice = 0; out_slice < num_slices; out_slice++) {
        int64_t kcur, k;
        int start = shard[in_slice][out_slice]->xoff[local_id];
        int end = shard[in_slice][out_slice]->xendoff[local_id];
        edge_t* edgelist = shard[in_slice][out_slice]->xadj;

        if (start+1 >= end) continue;
        qsort (&edgelist[start], end-start, sizeof(edge_t), edge_cmp);
        kcur = start;
        for (k = start+1; k < end; ++k) {
            if (edgelist[k].dest_id != edgelist[kcur].dest_id) {
                edgelist[++kcur] = edgelist[k];
            }
        }
        ++kcur;
        for (k = kcur; k < end; ++k)
            edgelist[k].dest_id = -1;
        shard[in_slice][out_slice]->xendoff[local_id] = kcur;
        num_neighbors += kcur - start;
    }
    return num_neighbors;
}


void Graph::gather_edges (const packed_edge * IJ, int64_t nedge) {
    int64_t k;
    for (k = 0; k < nedge; ++k) {
        vertexid_t i = partition->placement(IJ[k].v0);
        vertexid_t j = partition->placement(IJ[k].v1);

        if (i >= 0 && j >= 0 && i != j) {
            scatter_edge (i, j);
            if (!directed) scatter_edge (j, i);
        }
    }
    ne = 0;
    int64_t v;
    for (v = 0; v < nv; ++v) {
        ne += pack_vtx_edges (v);
    }
}

int Graph::num_neighbors(vertexid_t vertex, int out_slice){
    int in_slice = partition->slice_id(vertex);
    int local_id = partition->local_id(vertex);
    return shard[in_slice][out_slice]->num_neighbors(local_id);
}

int Graph::num_neighbors(vertexid_t vertex){
    int num_neighbors = 0;
    int in_slice = partition->slice_id(vertex);
    int local_id = partition->local_id(vertex);
    int out_slice;
    for (out_slice = 0; out_slice < num_slices; out_slice++) {
        num_neighbors += shard[in_slice][out_slice]->num_neighbors(local_id);
    }

    return num_neighbors;
}

edge_t Graph::get_neighbor(vertexid_t vertex, int index, int out_slice){
    int in_slice = partition->slice_id(vertex);
    int local_id = partition->local_id(vertex);
    return shard[in_slice][out_slice]->get_neighbor(local_id, index);
}


void Graph::print_dot(const char* fname){
    std::ofstream ofs;
    ofs.open (fname, std::ofstream::out);
    ofs << "graph last_graph {" << std::endl;
    int64_t v;
    for (v = 0; v < nv; ++v) {
        vertexid_t vname = partition->placement(v);
        int neighbors = 0;
        int slice;
        for (slice = 0; slice < num_slices; slice++) {
            int k;
            for (k = 0; k < num_neighbors(vname, slice); k++) {
                vertexid_t u = get_neighbor(vname, k, slice).dest_id;
                if (u > vname){
                    ofs << vname << " -- " << u << '\n';
                }
            }
            neighbors += num_neighbors(vname, slice);
        }

        if (neighbors == 0){
            ofs << vname << '\n';
        }
    }
    ofs << "}\n";
    ofs.close();
}
