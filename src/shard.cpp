#include "shard.h"
#include <stdexcept>
#include <iostream>

Shard::Shard(int shard_id_in, int shard_id_out, int num_vertices) : shard_id_in(shard_id_in), shard_id_out(shard_id_out), nv(num_vertices) {
    xoff = new int[num_vertices];
    xendoff = new int[num_vertices];
}

Shard::~Shard() {
    delete[] xoff;
    delete[] xendoff;
    delete[] xadj;
}

int Shard::num_neighbors(int local_id){
    // int in_slice = partition->slice_id(vertex);
    // int local_id = partition->local_id(vertex);
    // if (in_slice != shard_id_in) throw std::runtime_error("requesting neighbors of vertex not belonging to shard");
    int start = xoff[local_id];
    int end = xendoff[local_id];
    return end-start;
}

edge_t Shard::get_neighbor(int local_id, int index){
    // int in_slice = partition->slice_id(vertex);
    // int local_id = partition->local_id(vertex);
    // if (in_slice != shard_id_in) throw std::runtime_error("requesting neighbor of vertex not belonging to shard");
    int start = xoff[local_id];
    int end = xendoff[local_id];
    if (index < 0 || index > (end-start)) throw std::runtime_error("edge index out of bounds");
    return xadj[start+index];
}

void Shard::print_shard() {
    int k;
    std::cout << "Shard " << shard_id_in << " - "<< shard_id_out << ":\n";
    std::cout << "   xoff:";
    for (k = 0; k < nv; k++) {
        std::cout << " " << xoff[k];
    }
    std::cout << "\n";
    std::cout << "xendoff:";
    for (k = 0; k < nv; k++) {
        std::cout << " " << xendoff[k];
    }
    std::cout << "\n";

    std::cout << "   xadj:";
    for (k = 0; k < xendoff[nv - 1]; k++) {
        std::cout << " " << xadj[k].dest_id;
    }
    std::cout << "\n";
}
