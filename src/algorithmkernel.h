#include "format_def.h"

extern int superstep;

void receive(VertexEntry* vertex, Message* message);
bool send(Update* update, VertexEntry* vertex);
bool distribute(Message* message, Update* update, edge_t edge);
