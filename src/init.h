#pragma once

#include "graph.h"
#include "format_def.h"

void initVertexData(VertexEntry* init_data, vertexid_t vertex, Graph* graph);
void printFinalState(VertexEntry* init_data);
