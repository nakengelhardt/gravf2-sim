#pragma once

#include "user_def.h"
#include "platform_def.h"
#include <cstddef>

struct VertexEntry {
    vertexid_t id;
    VertexData data;
};

struct Message {
    vertexid_t sender;
    vertexid_t dest_id;
    MessagePayload payload;
};

struct Update {
    vertexid_t sender;
    UpdatePayload payload;
};

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
#define AT __FILE__ ":" TOSTRING(__LINE__) " : "
