#include "graph_partition.h"
#include <iostream>

int NODEID_MASK;
int PEID_SHIFT;

GraphPartition::GraphPartition(int nv){
    int num_vertex_per_pe = (nv + num_slices - 1)/num_slices;
    int localidsize = 1;
    while ((1 << localidsize) < num_vertex_per_pe)
        localidsize++;
    NODEID_MASK = (1 << localidsize) - 1;
    PEID_SHIFT = localidsize;

    for(int i = 0; i < nv; i++){
        vertexid_t vname = placement(i);
        int local = local_id(vname);
        int slice = slice_id(vname);

        if(local >= vertices_in_slice[slice]){
            vertices_in_slice[slice] = local + 1; // add 1 for exclusive bound
        }
        if (vertices_in_slice[slice] > max_vertices_in_slice) {
            max_vertices_in_slice = vertices_in_slice[slice];
        }
    }

    std::cout << "Vertices per slice: " << max_vertices_in_slice << " (PEID_SHIFT = " << PEID_SHIFT << ")"<< std::endl;
}

vertexid_t GraphPartition::placement(vertexid_t vertex){
    vertexid_t local_id = (vertex) / num_slices;
    vertexid_t slice_id = (vertex) % num_slices;
    return (slice_id << PEID_SHIFT) + local_id;
}

vertexid_t GraphPartition::origin(vertexid_t vertex){
    vertexid_t local_id = this->local_id(vertex);
    vertexid_t slice_id = this->slice_id(vertex);
    return origin(slice_id, local_id);
}

vertexid_t GraphPartition::origin(int slice_id, vertexid_t local_id){
    return (local_id * num_slices + slice_id);
}

int GraphPartition::slice_id(vertexid_t vertex){
    return (int) (vertex >> PEID_SHIFT);
}
int GraphPartition::local_id(vertexid_t vertex){
    return vertex & NODEID_MASK;
}
