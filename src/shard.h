#pragma once

#include "format_def.h"
#include "graph_partition.h"

class Shard {
    int shard_id_in;
    int shard_id_out;
    int nv;
public:
    int* xoff;
    int* xendoff;
    edge_t* xadj;
    Shard(int shard_id_in, int shard_id_out, int num_vertices);
    ~Shard();
    int num_neighbors(int local_id);
    edge_t get_neighbor(int local_id, int index);
    void print_shard();
};
